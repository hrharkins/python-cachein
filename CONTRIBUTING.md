
# HEADS UP

This is a pet project done on the maintainer's own time.  There are no 
warranties at all nor promises or commitments to accepting or working on 
changes.  The maintainer reserves the right to reject any submission for 
any reason (including and especially lack of time or interest).


# How to file a code submission

Updates will *only* be considered if:

* The change is provided in a pull reqeust via a branch.
   (Use python-compatible names.)
* A file exists in tests/ that reasonably resembles the branch name, starts
    with test_ and ends in .py.
* The branch name reasonably reflects the SOUTION.
* Running pre-pull-request testing below exits with status 0 on all Pythons.
* The change makes sense to me and does not affect the testing harnesses unduly.

I reserve the right to modify the update as I see fit for any reason prior
to inclusion.  I generally won't, but modifications might be made for example:

* To provide consistant style or naming.
* To allevaite obvious code issues.
* To adjust scope to match other changes in play.
* (Other possibilities abound)


# How to submit a bug

Bugs will be more likely worked on if:

* The description is clear and concise and either:
    * A new branch is created containing a tests/test_BRANCH_NAME.py file 
        that allows reproduction and demonstrates the issue, causing the 
        automated tests to produce non-zero results in devtest mode.
* OR IF NOT POSSIBLE:
    * There are clear reproduction steps.
    * There are clear descriptions of what was expected and why.
    * There are clear example snippets to demonstrate the problem.

Obviously, providing a test case that demonstrates the break is far more
likely to get attention than verbal details.

Neither case is a guarantee of work, however, and no obligation is extended
by the maintainer in any case.


# Tools for Development

## Development

To run the unit/doc/coverage tests, run:

$ docker-compose -f $PWD/docker/devel.yml up

The -f part can, of course, be replaced by a COMPOSE_FILE environment variable:

$ export COMPOSE_FILE=$PWD/docker/devel.yml
$ docker-compose up

A new folder, reports/, will contain details on the unittest and doctest
coverage.

If non-source files are changing (ones outside the source code or tests),
add --build to make sure things get rebuilt or simply run:

$ docker-compose -f $PWD/docker/devel.yml build

## Pre-pull-request testing

To run the doc/unit tests in quiet mode against various versions of Python:

$ docker-compose -f $PWD/docker/test.yml up --build

Look for status code 0 on all runs for success.  Errors are dumped on failure.

## Production deployment (if on your own fork)

After tagging in GitLabl, run:

$ VERSION=A.B.C docker-compose -f $PWD/docker/prod.yml run publish

This will download from the DOWNLOAD_URL in prod.yml, based on the VERSION
(tag) provided.  That image is checked to make sure the version matches
AND that the documentation path was set to match the version in README.md.

Occasionally, retagging will be needed, in which case a re-build is needed:

$ VERSION=A.B.C docker-compose -f $PWD/docker/prod.yml run build --no-cache
$ VERSION=A.B.C docker-compose -f $PWD/docker/prod.yml run publish
