# Cachein

Lazy members, methods, and more.

# Installation

$ pip install cachein

# Contributing / development

See CONTRIBUTING.md for how to submit changes and bugs, as well as the
development environment.

# Documentation

See [documentation here](http://glcdn.githack.com/hrharkins/python-cachein/raw/0.3.2/docs/html/cachein/index.html)

Or pydoc cachein
