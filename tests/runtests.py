#!/usr/bin/env python3.8
#
# Assumes pip install -r setup/test-requirements.txt has been run (or better
# yet is in docker).
#

import sys,  os
import unittest, coverage, doctest
import argparse, pathlib
import pkgutil, importlib

BASEDIR=pathlib.Path(
    os.path.dirname(os.path.dirname(os.path.realpath(sys.argv[0])))
)    

class Tester(object):
    def __init__(self, sources, report_dir):
        self.sources = sources
        self.report_dir = pathlib.Path(report_dir)
    
    def setup_syspath(self):
        # Make sure our code is top of the import path.
        if str(BASEDIR) not in sys.path:
            sys.path.insert(0, str(BASEDIR))
    
    def run(self):
        child_pid = os.fork()
        if child_pid:
            return (os.waitpid(child_pid, 0)[1] >> 8)
        else: 
            self.setup_syspath()
            print()
            print('#########################################################')
            print('# RUNNING %s' % type(self).__name__)
            print('#########################################################')
            print()
            runner = unittest.TextTestRunner()
            with CoverageContext(source=self.sources) as unittest_coverage:
                unittest_coverage.exclude('(NO-COV)')
                unittest_coverage.exclude(self.no_cov_marker)
                tests = self.get_tests()
                result = runner.run(tests)
            covered = unittest_coverage.report()
            if self.report_dir:
                report_path = pathlib.Path(self.report_dir)
                unittest_coverage.html_report(
                    directory=str(report_path / self.coverage_subdir)
                )
            sys.exit(not (covered < 100.0) and result.wasSuccessful())

class DocTester(Tester):
    no_cov_marker = '(COV-NO-DT)'
    coverage_subdir = 'coverage/doctest/html'
    
    def get_tests(self):
        suite = unittest.TestSuite()
        # TODO: Find a better means of auto-generating these.
        importlib.invalidate_caches()
        for source in self.sources:
            for finder, modname, ispkg in pkgutil.walk_packages([source]):
                suite.addTests(doctest.DocTestSuite(source + '.' + modname))
        return suite
        
class UnitTester(Tester):
    no_cov_marker = '(COV-NO-DT)'
    coverage_subdir = 'coverage/unittest/html'
    
    def get_tests(self):
        importlib.invalidate_caches()

        runner = unittest.TextTestRunner()
        tests = unittest.TestLoader().discover(BASEDIR / 'tests')
        return tests

class CoverageContext(coverage.Coverage):
    def __enter__(self):
        self.erase()
        self.start()
        return self
        
    def __exit__(self, *_args, **_kw):
        self.stop()
    
def main(args):
    parser = build_arg_parser()
    parser.add_argument('-q', dest='quiet', action='store_true')
    parser.add_argument('sources', action='append')
    parsed = parser.parse_args(args)
    
    if parsed.quiet:
        sys.stdout = open(os.devnull, 'w')
    
    testdirs = args
   
    # Status is OS style 0-okay style status.   Run both regardless of status
    # but report a non-zero unless both are zero.
    status = UnitTester(parsed.sources, parsed.report).run()
    status = status or DocTester(parsed.sources, parsed.report).run()
        
    return status        

def build_arg_parser(parser=None):
    if parser is None:
        parser = argparse.ArgumentParser()
        
    parser.add_argument('--report', dest='report')        

    return parser

if __name__ == '__main__':
    result = main(sys.argv[1:])
    if result is True or result is None:
        sys.exit(0)
    elif result is False or not isinstance(result, int):
        sys.exit(1)
    else:
        sys.exit(result)
