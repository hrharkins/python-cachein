
import unittest, cachein

class TestCaching(unittest.TestCase):
    def test_member(self):
        class Calculator(object):
            calculated = 0
            
            @cachein.Cached()
            def cached(self):
                self.calculated += 1
                return 'RESULT'

        c = Calculator()
        self.assertEqual(c.cached, 'RESULT')
        self.assertEqual(c.cached, 'RESULT')
        self.assertEqual(c.calculated, 1)
        
        del c.cached
        self.assertEqual(c.cached, 'RESULT')
        self.assertEqual(c.cached, 'RESULT')
        self.assertEqual(c.calculated, 2)

    def test_method(self):
        class Calculator(object):
            calculated = 0
            
            @cachein.CachedAs('cached')
            def get_cached(self):
                self.calculated += 1
                return 'RESULT'
      
        c = Calculator()
        self.assertEqual(c.cached, 'RESULT')
        self.assertEqual(c.cached, 'RESULT')
        self.assertEqual(c.calculated, 1)
        
        self.assertEqual(c.get_cached(), 'RESULT')
        self.assertEqual(c.calculated, 2)
        
        del c.cached
        self.assertEqual(c.cached, 'RESULT')
        self.assertEqual(c.cached, 'RESULT')
        self.assertEqual(c.calculated, 3)

    def test_classget(self):
        class Stub(object):
            @cachein.Cached()
            def stub1(self):
                pass
            
            @cachein.CachedAs('stubattr')
            def stub2(self):
                pass
        
        self.assertIsInstance(Stub.stub1, cachein.Cached)
        self.assertIsInstance(Stub.stubattr, cachein.CachedAs)

    def test_clonefactory(self):
        class Break(object):
            @cachein.Cached()
            def stub(self):
                return 'STUB'

        with self.assertRaises(TypeError):
            Break.stub.clone(clonefactory=int)
            
        Break.stub.clone(clonefactory=cachein.CachedAs)

class TestAttrProxy(unittest.TestCase):
    def test_implicit(self):
        class Source(object):
            def fn(self):
                return 'SOURCE'
            attr = 'ATTR'
            
        class DirectExtractor(object):
            def __init__(self, source):
                self.source = source
                
            fn = attr = cachein.AttrProxy('source')
            renamed_fn = cachein.AttrProxy('source', 'fn')
            renamed_attr = cachein.AttrProxy('source', 'attr')

        obj = DirectExtractor(Source())
        self.assertEqual(obj.fn(), 'SOURCE')
        self.assertEqual(obj.renamed_fn(), 'SOURCE')
        self.assertEqual(obj.attr, 'ATTR')
        self.assertEqual(obj.renamed_attr, 'ATTR')

    def test_explict(self):
        class Source(object):
            def fn(self):
                return 'SOURCE'
            attr = 'ATTR'
            
        class AttrProxyExtractor(object):
            @cachein.Cached()
            def source(self):
                return Source()
                
            fn = attr = cachein.AttrProxy(source)
            renamed_fn = cachein.AttrProxy(source, 'fn')
            renamed_attr = cachein.AttrProxy(source, 'attr')

        obj = AttrProxyExtractor()
        self.assertEqual(obj.fn(), 'SOURCE')
        self.assertEqual(obj.renamed_fn(), 'SOURCE')
        self.assertEqual(obj.attr, 'ATTR')
        self.assertEqual(obj.renamed_attr, 'ATTR')
       
class TestComposition(unittest.TestCase):
    def test_explicit(self):
        class Source(object):
            def fn(self):
                return 'fn'
            attr = 'attr'
        
        @cachein.AttrProxy.compose('src', 'fn', 'attr')
        class Composed(object):
            def __init__(self, src):
                self.src = src
        
        obj = Composed(Source())
        self.assertEqual(obj.fn(), 'fn')
        self.assertEqual(obj.attr, 'attr')
                
    def test_renamed(self):
        class Source(object):
            def fn(self):
                return 'fn'
            attr = 'attr'
        
        @cachein.AttrProxy.compose('src', the_attr='attr', the_fn='fn')
        class Composed(object):
            def __init__(self, src):
                self.src = src
        
        obj = Composed(Source())
        self.assertEqual(obj.the_fn(), 'fn')
        self.assertEqual(obj.the_attr, 'attr')
                
    def test_composer(self):
        class Source(object):
            def fn(self):
                return 'fn'
            attr = 'attr'
            
        anySource = cachein.AttrProxy.composer('fn', 'attr')
        
        @anySource.src()
        class Composed(object):
            def __init__(self, src):
                self.src = src
        
        obj = Composed(Source())
        self.assertEqual(obj.fn(), 'fn')
        self.assertEqual(obj.attr, 'attr')
                
    def test_composer_with_mods(self):
        class Source(object):
            def fn(self):
                return 'fn'
            attr = 'attr'
            
        anySource = cachein.AttrProxy.composer('fn', 'attr')
        
        @anySource.src(the_fn='fn', attr=False)
        class Composed(object):
            def __init__(self, src):
                self.src = src
        
        obj = Composed(Source())
        self.assertEqual(obj.the_fn(), 'fn')
        with self.assertRaises(AttributeError):
            obj.attr
                
class TestManager(unittest.TestCase):
    def test_cached(self):
        class Calculator(object):
            @cachein.Cached()
            def cached(self):
                return 'RESULT'

        c = Calculator()
        mgr = cachein.CacheManager(c)
        self.assertFalse('cached' in mgr)
        self.assertEqual(c.cached, 'RESULT')
        self.assertTrue('cached' in mgr)
                
    def test_subclass(self):
        class Calculator(cachein.CacheinClass):
            @cachein.Cached()
            def cached(self):
                return 'RESULT'

        c = Calculator()
        self.assertFalse('cached' in c.__cachein__)
        self.assertEqual(c.cached, 'RESULT')
        self.assertTrue('cached' in c.__cachein__)
    
    def test_clear(self):
        class Calculator(object):
            @cachein.Cached()
            def cached(self):
                return 'RESULT'

        c = Calculator()
        mgr = cachein.CacheManager(c)
        self.assertFalse('cached' in mgr)
        self.assertEqual(c.cached, 'RESULT')
        self.assertTrue('cached' in mgr)
        
        del mgr.cached
        self.assertFalse('cached' in mgr)
        self.assertEqual(c.cached, 'RESULT')
        self.assertTrue('cached' in mgr)
        
        del mgr['cached']
        self.assertFalse('cached' in mgr)
        self.assertEqual(c.cached, 'RESULT')
        self.assertTrue('cached' in mgr)
        
    def test_getvalue(self):
        class Calculator(object):
            @cachein.Cached()
            def cached(self):
                return 'RESULT'

        c = Calculator()
        mgr = cachein.CacheManager(c)
        
        self.assertIsNone(mgr['cached':])
        self.assertEquals(mgr['cached':'default'], 'default')
        with self.assertRaises(KeyError):
            self.assertEqual(mgr['cached'], 'RESULT')
        with self.assertRaises(AttributeError):
            self.assertEqual(mgr.cached, 'RESULT')
        
        self.assertEqual(c.cached, 'RESULT')
        
        self.assertEqual(mgr['cached'], 'RESULT')
        self.assertEqual(mgr.cached, 'RESULT')

    def test_preset_attr(self):
        class Calculator(object):
            @cachein.Cached()
            def cached(self):
                return 'RESULT'

        c = Calculator()
        mgr = cachein.CacheManager(c)
        mgr.cached = 'Hello'
        self.assertEqual(c.cached, 'Hello')
        
    def test_preset_item(self):
        class Calculator(object):
            @cachein.Cached()
            def cached(self):
                return 'RESULT'

        c = Calculator()
        mgr = cachein.CacheManager(c)
        mgr['cached'] = 'Hello'
        self.assertEqual(c.cached, 'Hello')

    def test_really_cached_not_attr(self):
        class Calculator(object):
            def __init__(self):
                self.fake = 'FAKE'
            
            @cachein.Cached()
            def cached(self):
                return 'RESULT'

        c = Calculator()
        mgr = cachein.CacheManager(c)
        self.assertFalse('cached' in mgr)
        with self.assertRaises(ValueError):
            mgr.fake
        self.assertEqual(c.cached, 'RESULT')
        self.assertTrue('cached' in mgr)
        with self.assertRaises(ValueError):
            mgr.fake
        
        with self.assertRaises(ValueError):
            mgr.fake
        with self.assertRaises(ValueError):
            mgr['fake']
        with self.assertRaises(ValueError):
            mgr['fake':]
        with self.assertRaises(ValueError):
            mgr.fake = True
        with self.assertRaises(ValueError):
            mgr['fake'] = True
        with self.assertRaises(ValueError):
            del mgr['fake']
        with self.assertRaises(ValueError):
            del mgr.fake
        with self.assertRaises(ValueError):
            'fake' in mgr
