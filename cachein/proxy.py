
from .member import Cached
import abc

NOT_FOUND = type('NOT_FOUND', (str,), {})('NOT_FOUND')

class Proxy(Cached, abc.ABC):
    '''
    Base class for Proxy delegates.  See AttrProxy, KeyProxy, etc, for
    more details.
    '''
    
    def __init__(self, source, attr=None, **_kw):
        super().__init__(source=source, attr=attr, **_kw)
        
    def init(self, source, attr, **_kw):
        super().init(**_kw)
        self.source = source
        self.attr = attr

    def getter(proxy, NOT_FOUND=NOT_FOUND):
        source = getattr(proxy.source, '__cachein_delegate_attr__', proxy.source)
            
        def delegate_getter(self, target, cls=None, name=proxy.name,
                            source=source, attr=proxy.attr or proxy.name):
            if target is None:
                return proxy     # (NO-COV-DT)
            else:
                obj = proxy.extract(getattr(target, source), attr, NOT_FOUND)
                target.__dict__[name] = obj
                return obj
        return delegate_getter

    @abc.abstractmethod        
    def extract(self, obj, name):
        '''
        Extracts the name from obj inwhatever way works for the proxy.
        '''

    @classmethod
    def compose(_cls, _source, *_args, **_kw):
        def decorator(cls):
            for arg in _args:
                if _kw.get(arg, True):
                    setattr(cls, arg, _cls(_source, arg, name=arg))
            for dest, attr in _kw.items():
                if attr:
                    setattr(cls, dest, _cls(_source, attr, name=dest))
            return cls                
        return decorator

    @classmethod
    def composer(_cls, *_args, **_kw):
        return _cls.Composer(_cls, _args, _kw)

    class Composer(object):
        def __init__(self, cls, args, kw):
            self.__cls = cls
            self.__args = args
            self.__kw = kw
            
        def __getattr__(self, name):
            return lambda *_a, **_kw: self.compose(name, *_a, **_kw)
            
        def compose(_self, _source, *_args, **_kw):
            args = _self.__args + _args
            kw = dict(_self.__kw, **_kw)
            return _self.__cls.compose(_source, *args, **kw)

class AttrProxy(Proxy):
    '''
    A caching delegate.  This brings a member or method into the containing
    instance lazily when accessed.
    
    >>> import cachein
    >>> class Greeter(object):
    ...     def __init__(self, recipient):
    ...         self.recipient = recipient
    ...
    ...     def greet(self):
    ...         return 'Hello, %s' % self.recipient
    
    >>> class Helper(object):
    ...     def __init__(self, greeter):
    ...         self.greeter = greeter
    ...
    ...     greet = recipient = AttrProxy('greeter')

    >>> helper = Helper(Greeter('world'))
    >>> helper.greet()
    'Hello, world'
    >>> helper.recipient
    'world'
    
    >>> class RecipientHelper(object):
    ...     def __init__(self, recipient='world', greetercls=Greeter):
    ...         self.recipient = recipient
    ...         self.greetercls = greetercls
    ...
    ...     @cachein.Cached()
    ...     def greeter(self):
    ...         return self.greetercls(self.recipient)
    ...     greet = AttrProxy(greeter)
    ...     who = AttrProxy(greeter, 'recipient')

    >>> RecipientHelper().greet()
    'Hello, world'
    >>> RecipientHelper('everyone').greet()
    'Hello, everyone'
    >>> RecipientHelper('everyone').who
    'everyone'

    >>> @AttrProxy.compose('greeter', 'greet', who='recipient')
    ... class ComposedHelper(object):
    ...     def __init__(self, greeter):
    ...         self.greeter = greeter
    
    >>> ComposedHelper(Greeter('world')).greet()
    'Hello, world'
    >>> ComposedHelper(Greeter('world')).who
    'world'
    
    >>> anyGreeter = AttrProxy.composer('greet', who='recipient')
    >>> @anyGreeter.greeter('recipient', 'recipient', who=False)
    ... class AnyGreeterHelper(object):
    ...     def __init__(self, greeter):
    ...         self.greeter = greeter
    >>> AnyGreeterHelper(Greeter('world')).greet()
    'Hello, world'
    >>> AnyGreeterHelper(Greeter('world')).recipient
    'world'
    '''
    
    def extract(self, obj, name, not_found):
        return getattr(obj, name, not_found)

class KeyProxy(Proxy):
    '''
    Similar to AttrProxy, but pulls keys via getitem.  
    
    >>> class DictNamecard(dict):
    ...     def __getitem__(self, name):
    ...         print('Getting %r' % name)
    ...         return super().__getitem__(name)
    
    >>> class DictGreeter(object):
    ...     def __init__(self, namecard):
    ...         self.namecard = namecard 
    ... 
    ...     name = KeyProxy('namecard')
    
    A regular dictionary would be fine too, using DictNamecard to demonstrate
    that [] isn't called a second time on the proxied dictionary.
    
    >>> namecard = DictNamecard(name='everyone')
    >>> greeter = DictGreeter(namecard)
    >>> greeter.name
    Getting 'name'
    'everyone'
    >>> greeter.name
    'everyone'
    
    '''
    
    def extract(self, obj, key, not_found):
        return obj[key]
