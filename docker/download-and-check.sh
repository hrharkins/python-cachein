#!/bin/bash

function main
{
    local download_url="${DOWNLOAD_URL}"
    local dir="."
    while getopts 'd:u:' opt
    do
        case "$opt" in
        u) download_url="$OPTARG";;
        d) dir="$OPTARG";;
        esac
    done
    shift $(( OPTIND - 1 ))
    
    [ "$download_url" ] || fatal 65 "A download URL is required."
    local version="$1"; shift || fatal 65 "A version is required."
    local download_url="${download_url//%VERSION%/$version}"
    
    mkdir -p "$dir" && cd "$dir" &&
    download "$download_url" &&
    check_setup_version "$version" &&
    check_readme_version "$version" &&
    check_index_version "$version"
}

function download
{
    local url="$1"; shift || fatal 65 "A url is required."
    curl "$url" | tar xfvz - --strip-components=1
}

function check_setup_version
{
    local version="$1"; shift || fatal 65 "A version is required."
    VERSION="$version" PYTHON_PATH="." \
    python3 -c 'import cachein, os; \
                assert cachein.VERSION == os.environ["VERSION"], \
                       ("cachein.VERSION = %r != VERSION = %r)" 
                        % (cachein.VERSION, os.environ["VERSION"]))
    '
}

function check_readme_version
{
    local version="$1"; shift || fatal 65 "A version is required."
    fgrep -q "/${version}/" README.md ||
        fatal 1 "README.md needs updated (/${version}/ not found)!"
}

function check_index_version
{
    local version="$1"; shift || fatal 65 "A version is required."
    fgrep -q "&#39;${version}&#39;" docs/html/*/index.html ||
        fatal 1 "('${version}' not found in index -- were docs updated?)!"
}

function fatal
{
    local rc="$1"; shift || fatal 1 "fatal without return code."
    local msg="$1"; shift || fatal 1 "fatal likely missing return code."
    echo >&2 "$msg in $(caller 1)"
    exit $rc
}

main "$@"; exit $?
